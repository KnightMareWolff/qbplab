#include "QBP_TabPage3DCharts.h"

CQBPEngine * CQBPTabPage3DCharts::pQBPEngine=nullptr;

CQBPTabPage3DCharts::CQBPTabPage3DCharts(QWidget *parent) : QWidget(parent)
{
    pQBPChartsGrid   = new QGridLayout(parent);
    pQBPLoadProgress = new QProgressBar();


    m_fontSize= 40.0f;
    m_style = QAbstract3DSeries::MeshSphere;
    m_smooth = true;

    QPushButton *LoadCharts   = new QPushButton(tr("Load Charts"));
    QPushButton *SaveCharts   = new QPushButton(tr("Save Charts"));

    //Grupo de Criação
    QGroupBox *GrupoChartParameters = new QGroupBox(tr("Chart Options:"));

    QHBoxLayout *LayoutOptCharts = new QHBoxLayout;
    LayoutOptCharts->addWidget(LoadCharts);
    LayoutOptCharts->addWidget(SaveCharts);
    QVBoxLayout *LayoutProgress = new QVBoxLayout;
    LayoutProgress->addLayout(LayoutOptCharts);
    LayoutProgress->addWidget(pQBPLoadProgress);

    GrupoChartParameters->setLayout(LayoutProgress);

    //Inclui os Grupos no Layout Principal
    QVBoxLayout *LayoutPrincipal = new QVBoxLayout;
    LayoutPrincipal->addWidget(GrupoChartParameters);
    LayoutPrincipal->addLayout(pQBPChartsGrid);

    //Set the Main Layout of the Grid of Charts
    setLayout(LayoutPrincipal);

    //stablish controls connection
    connect(LoadCharts , SIGNAL(clicked()), this, SLOT(OnBtnLoadCharts()));
    connect(SaveCharts , SIGNAL(clicked()), this, SLOT(OnBtnSaveCharts()));

    pQBPScatter    = new Q3DScatter();

    pQBPScatterContainer = QWidget::createWindowContainer(pQBPScatter);
    if (!pQBPScatter->hasContext()) {
        QMessageBox msgBox;
        msgBox.setText("Couldn't initialize the OpenGL context.");
        msgBox.exec();
    }

    QSize screenSize = size();
    pQBPScatterContainer->setMinimumSize(QSize(screenSize.width() / 2, screenSize.height() / 1.5));
    pQBPScatterContainer->setMaximumSize(screenSize);
    pQBPScatterContainer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    pQBPScatterContainer->setFocusPolicy(Qt::StrongFocus);


    pQBPScatter->activeTheme()->setType(Q3DTheme::ThemeStoneMoss);
    QFont font = pQBPScatter->activeTheme()->font();

    font.setPointSize(m_fontSize);
    pQBPScatter->activeTheme()->setFont(font);
    pQBPScatter->setShadowQuality(QAbstract3DGraph::ShadowQualitySoftLow);
    pQBPScatter->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetFront);


    QScatterDataProxy *proxy = new QScatterDataProxy;
    QScatter3DSeries *series = new QScatter3DSeries(proxy);
    series->setItemLabelFormat(QStringLiteral("@xTitle: @xLabel @yTitle: @yLabel @zTitle: @zLabel"));
    series->setMeshSmooth(m_smooth);
    pQBPScatter->addSeries(series);

    //Change the mesh model
    m_style = QAbstract3DSeries::Mesh(int(m_style));
    if (pQBPScatter->seriesList().size())
    {
        pQBPScatter->seriesList().at(0)->setMesh(m_style);
        pQBPScatter->seriesList().at(0)->setBaseColor(QColor(150,50,10));
    }


    //Add the chart on grid
    pQBPChartsGrid->addWidget(pQBPScatterContainer);

}

void CQBPTabPage3DCharts::setEngineReference(CQBPEngine *pQBP_Engine)
{
    pQBPEngine = pQBP_Engine;
}

void CQBPTabPage3DCharts::OnBtnSaveCharts()
{
    QString fileName = "sauve.jpg";
    fileName = QFileDialog::getSaveFileName(this,"Sauve...","..."," (*.jpg)");
    if(!fileName.isEmpty())
    {
    QPixmap picture;
    //picture = QPixmap::grabWidget(pQBPScatterContainer);
    picture = grab();
    picture.save(fileName);
    }
}
void CQBPTabPage3DCharts::OnBtnLoadCharts()
{
    addData();
}

void CQBPTabPage3DCharts::addData()
{
    int tQtdGenres=0;
    int tQtdSongs=0;
    int tProgress=0;
    int tQtdValues=0;
    // Configure the axes according to the data
    pQBPScatter->axisX()->setTitle("BPMMedio");
    pQBPScatter->axisY()->setTitle("Attitude");
    pQBPScatter->axisZ()->setTitle("NumberOfPitchs");

    tQtdGenres = pQBPEngine->GenresLoaded();
    tQtdSongs  = pQBPEngine->SongsLoaded();

    for (float x = 0; x < tQtdGenres; x++)
    {
        QString tGenreName;
        int     tGenreQtdSongs;

        pQBPEngine->GetGenreData(x,tGenreName,tGenreQtdSongs);

        for (float y = 0; y < tGenreQtdSongs; y++)
        {
            tQtdValues++;
        }
    }

    pQBPLoadProgress->setMinimum(0);
    QScatterDataArray *dataArray = new QScatterDataArray;
    dataArray->resize(tQtdValues);
    QScatterDataItem *ptrToDataArray = &dataArray->first();
    pQBPLoadProgress->setMaximum(tQtdValues);

    for (float x = 0; x < tQtdGenres; x++)
    {
        QString tGenreName;
        int     tGenreQtdSongs;

        pQBPEngine->GetGenreData(x,tGenreName,tGenreQtdSongs);

        for (float y = 0; y < tGenreQtdSongs; y++)
        {
            QString tSongName;
            int     tQtdSongNotes;
            int     tQtdSongFeatures;
            int     tQtdSongCustomFeatures;
            QString tQBPFeatureName;
            int     tQBPFeatureType;
            double  tQBPFeatureValue;

            int tBPMMedio;
            int tAttitude;
            int tNumberOfPitchs;

            pQBPEngine->GetSongData(x,y,tSongName,tQtdSongNotes,tQtdSongFeatures,tQtdSongCustomFeatures);

            for(int k=0;k<tQtdSongCustomFeatures;k++)
            {
                pQBPEngine->GetFeatureData(x,y,k,tQBPFeatureName,tQBPFeatureType,tQBPFeatureValue);
                if(tQBPFeatureName=="NumerodePitchs"  )
                {
                    tNumberOfPitchs       = tQBPFeatureValue;
                }
                if(tQBPFeatureName=="BPMMedio" )
                {
                    tBPMMedio = tQBPFeatureValue;
                }
                if(tQBPFeatureName=="AtitudedoMusico")
                {
                    tAttitude      = tQBPFeatureValue;
                }
            }
            ptrToDataArray->setPosition(QVector3D(tBPMMedio,tAttitude,tNumberOfPitchs));
            ptrToDataArray++;
            tProgress++;
            pQBPLoadProgress->setValue(tProgress);
        }
    }

    pQBPScatter->seriesList().at(0)->dataProxy()->resetArray(dataArray);

}

void CQBPTabPage3DCharts::resizeEvent(QResizeEvent *event)
{
    if(pQBPScatter     ==nullptr ||
       pQBPScatterContainer ==nullptr )
    {
        return;
    }

    QSize screenSize = size();
    pQBPScatterContainer->setMinimumSize(QSize(screenSize.width() / 2, screenSize.height() / 1.5));
    pQBPScatterContainer->setMaximumSize(screenSize);
    pQBPScatterContainer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    pQBPScatterContainer->setFocusPolicy(Qt::StrongFocus);
}


