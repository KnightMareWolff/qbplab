#include "QBP_MatrixValue.h"



CQBPMatrixValue::CQBPMatrixValue()
{
    iQBPRefX     = 0;
    iQBPRefY     = 0;
    iQBPValue     = 0;
    iQBPDirection = 0;
    bQBPMatch = false;
}

CQBPMatrixValue::CQBPMatrixValue(const CQBPMatrixValue &obj)
{
    iQBPRefX      = obj.iQBPRefX;
    iQBPRefY      = obj.iQBPRefY;
    iQBPValue     = obj.iQBPValue;
    iQBPDirection = obj.iQBPDirection;
    bQBPMatch     = obj.bQBPMatch;
}

CQBPMatrixValue & CQBPMatrixValue::operator = (const CQBPMatrixValue& other)
{
    iQBPRefX      = other.iQBPRefX;
    iQBPRefY      = other.iQBPRefY;
    iQBPValue     = other.iQBPValue;
    iQBPDirection = other.iQBPDirection;
    bQBPMatch     = other.bQBPMatch;
    return *this;
}

CQBPMatrixValue::CQBPMatrixValue(int pQBPValue, int pQBPDirection , bool pQBPMatch )
{
    iQBPRefX     = 0;
    iQBPRefY     = 0;
    iQBPValue = pQBPValue;
    iQBPDirection = pQBPDirection;
    bQBPMatch = pQBPMatch;
}

CQBPMatrixValue::CQBPMatrixValue(int pQBPValue, int pQBPDirection , bool pQBPMatch , int pRefX,int pRefY)
{
    iQBPRefX       = pRefX;
    iQBPRefY       = pRefY;
    iQBPValue      = pQBPValue;
    iQBPDirection  = pQBPDirection;
    bQBPMatch      = pQBPMatch;
}

CQBPMatrixValue::~CQBPMatrixValue()
{
}
